+
+# Badestellen app
+## What is it
+kurze Einführung um was es in dieser Readme geht und eventuel eine kleine Beschreibung der features
+
+
+## Dokumentation
+
+Solte sie existieren, ein paar Links zu mehr detaillierten Dokumentationen welche sich mit dem Projekt beschäftigen.
+
+## Installation
+ein kurzes Walk through, durch den Installationsprozess. Wie kann das Projekt gestartet werden, welche Fehler können auftreten etc.
+
+## License
+Anzeigen der Lizenz unter welcher das Programm steht
+
+## Contacts
+
+Informationen über den Autor und eventuell seine Kontaktdaten