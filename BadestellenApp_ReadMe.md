
# Badestellen app
## What is it
Diese App soll alle Badestellen in Berlin beinhalten, und soll für die User es ermöglichen auch mobil Informationen über naheliegende Badestellen Informationen geben.Die App soll sowohl online als auch offline arbeiten können und somit für alle User benutzerfreundlich sein.

## Dokumentation

Solte sie existieren, ein paar Links zu mehr detaillierten Dokumentationen welche sich mit dem Projekt beschäftigen.

[Konzept](https://bitbucket.org/aass16team04/techmobsys/commits/446509dd413f71d4acb04c2c17df18c48028053e#chg-Konzept_techMob.pdf)

## Installation
Da keine lauffähige APK dem Projekt beigelegt wurde, muss diese erzeugt werden. Nähere Informationen, wie eine APK erfolgreich installiert werden kann findet man [hier](https://developer.android.com/studio/run/index.html).

Bei der Installation traten schwerwiegende Fehler auf, die nicht ohne einen entsprechenden Zeitaufwand behoben werden konnten.


## License
Anzeigen der Lizenz unter welcher das Programm steht

## Contacts

Informationen über den Autor und eventuell seine Kontaktdaten
